package academy.learnprogramming.flashkart;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.Test;

import static org.junit.Assert.*;
public class CardTest {

    Card card = new Card("card_id" , 1, "question" , "answer");

    @Test
    public void getCardIndex() {
        int index = card.getCardIndex();
        assertEquals(index, 1);
    }

    @Test
    public void setCardIndex() {
        card.setCardIndex(1);
        assertNotEquals(0,1);
    }

    @Test
    public void getQuestion() {
        String question = card.getQuestion();
        assertEquals(question, "question");
    }

    @Test
    public void setQuestion() {
        card.setQuestion("newQuestion");
        assertNotEquals("question","newQuestion");
    }

    @Test
    public void getAnswer() {
        String answer = card.getAnswer();
        assertEquals(answer, "answer");
    }

    @Test
    public void setAnswer() {
        card.setAnswer("newAnswer");
        assertNotEquals("answer","newAnswer");
    }

    @Test
    public void getCardID() {
        String id = card.getCardID();
        assertEquals(id, "card_id");
    }

    @Test
    public void setCardID() {
        card.setAnswer("card_id2");
        assertNotEquals("card_id","card_id2");
    }
}