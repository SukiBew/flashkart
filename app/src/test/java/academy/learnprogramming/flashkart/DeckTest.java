package academy.learnprogramming.flashkart;

import org.junit.Test;

import static org.junit.Assert.*;

public class DeckTest {

    Deck deck = new Deck("deck_id" , "deck_name", 0);

    @Test
    public void getDeckSize() {
        int index = deck.getDeckSize();
        assertEquals(index, 0);
    }

    @Test
    public void setDeckSize() {
        deck.setDeckSize(1);
        assertNotEquals(0,1);
    }

    @Test
    public void getDeckName() {
        String deckName = deck.getDeckName();
        assertEquals( deckName, "deck_name");
    }

    @Test
    public void setDeckName() {
        deck.setDeckName("new_deck_name");
        assertNotEquals("deck_name","new_deck_name");
    }

    @Test
    public void getId() {
        String deck_id = deck.getId();
        assertEquals(deck_id, "deck_id");
    }

    @Test
    public void setId() {
        deck.setId("new_deck_id");
        assertNotEquals("deck_id" ,"new_deck_id");
    }
}