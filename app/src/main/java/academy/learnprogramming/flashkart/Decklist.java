package academy.learnprogramming.flashkart;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class Decklist extends ArrayAdapter<Deck> {
    private Activity context;
    private List<Deck> deckList;

    public Decklist(Activity context, List<Deck> deckList) {
        super(context, R.layout.decklist_layout, deckList);
        this.context = context;
        this.deckList = deckList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.decklist_layout, null, true);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textView_decknamelist);

        Deck deck = deckList.get(position);

        textViewName.setText(deck.getDeckName());
        return listViewItem;
    }
}
