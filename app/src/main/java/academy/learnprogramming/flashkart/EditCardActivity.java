package academy.learnprogramming.flashkart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class EditCardActivity extends AppCompatActivity {

    public static final String TAG = "Flashkart";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";
    public static final String CARD_ID = "cardID";
    public static final String CARD_INDEX = "cardIndex";
    public static final String CARD_QUESTION = "question";
    public static final String CARD_ANSWER = "answer";

    TextView textView_EditCardDeckName;
    TextView textView_EditCardQuestion;
    TextView textView_EditCardAnswer;
    EditText editText_EditCardQuestion;
    EditText editText_EditCardAnswer;
    Button button_EditCardSave;

    DatabaseReference databaseDeck;
    DatabaseReference databaseCards;
    List<Card> cardList;

    private String new_question;
    private String new_answer;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_card);

        //initialise every widgets
        textView_EditCardDeckName = (TextView) findViewById(R.id.textView_EditCardDeckName);
        textView_EditCardQuestion = (TextView) findViewById(R.id.textView_EditCardQuestion);
        textView_EditCardAnswer = (TextView) findViewById(R.id.textView_EditCardAnswer);
        editText_EditCardQuestion = (EditText) findViewById(R.id.editText_EditCardQuestion);
        editText_EditCardAnswer = (EditText) findViewById(R.id.editText_EditCardAnswer);
        button_EditCardSave = (Button) findViewById(R.id.button_EditCardSave);
        cardList = new ArrayList<>();

        //Retrieving all of the passed data from the previous activity
        Intent intent = getIntent();
        final String card_id = intent.getStringExtra(EditMenuActivity.CARD_ID);
        final String card_answer = intent.getStringExtra(EditMenuActivity.CARD_ANSWER);
        final String card_question = intent.getStringExtra(EditMenuActivity.CARD_QUESTION);
        final int card_index = intent.getIntExtra(EditMenuActivity.CARD_INDEX, 0);
        final String deckname = intent.getStringExtra(EditMenuActivity.DECK_NAME);
        final String deckid = intent.getStringExtra(EditMenuActivity.DECK_ID);

        //set the default text for textview
        textView_EditCardDeckName.setText(deckname);
        textView_EditCardQuestion.setText("Question:");
        textView_EditCardAnswer.setText("Answer:");
        editText_EditCardAnswer.setText(card_answer);
        editText_EditCardQuestion.setText(card_question);

        //set a listener for button save
        button_EditCardSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //retrieve information from edit texts
                new_question = editText_EditCardQuestion.getText().toString();
                new_answer = editText_EditCardAnswer.getText().toString();

                //forces the user to fill the edit text widgets
                if (!TextUtils.isEmpty(new_question) && !TextUtils.isEmpty(new_answer)) {
                    //create an empty card instance
                    Card card = new Card();

                    //update the cards in the database
                    card.updateCard(card_id, card_index, new_question, new_answer, deckid);
                    Toast.makeText(v.getContext(), "Card updated", Toast.LENGTH_SHORT).show();
                    //move to home activity
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                }
                else {
                    //notify user
                    Toast.makeText(v.getContext(), "Question or Answer should not be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
