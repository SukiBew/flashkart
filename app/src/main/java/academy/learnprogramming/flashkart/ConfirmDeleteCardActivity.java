package academy.learnprogramming.flashkart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class ConfirmDeleteCardActivity extends AppCompatActivity {

    public static final String TAG = "Flashkart-ConfirmDelete";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";
    public static final String CARD_ID = "cardID";
    public static final String CARD_INDEX = "cardIndex";
    public static final String CARD_QUESTION = "question";
    public static final String CARD_ANSWER = "answer";

    DatabaseReference databaseCards;
    DatabaseReference databaseDecks;
    DatabaseReference databaseCardlist;
    DatabaseReference databaseTest;

    TextView textView_ConfirmDeleteCardQTAG;
    TextView textView_ConfirmDeleteCardQfill;
    TextView textView_ConfirmDeleteCardATAG;
    TextView textView_ConfirmDeleteCardAfill;
    TextView textView_ConfirmDeleteCardWarning;
    Button button_ConfirmDeleteCardConfirm;
    Button button_ConfirmDeleteCardCancel;
    List<Card> cardList;
    List<String> card_id_list;

    private int dynamic_size;
    private String removed_id = null;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: in");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_delete_card);

        //retrieve information from previous activities
        Intent intent = getIntent();
        final String deck_id = intent.getStringExtra(DeleteCardActivity.DECK_ID);
        final String deck_name = intent.getStringExtra(DeleteCardActivity.DECK_NAME);
        final int deck_size = intent.getIntExtra(DeleteCardActivity.DECK_SIZE, 0);
        final String card_id = intent.getStringExtra(DeleteCardActivity.CARD_ID);
        String card_answer = intent.getStringExtra(DeleteCardActivity.CARD_ANSWER);
        String card_question = intent.getStringExtra(DeleteCardActivity.CARD_QUESTION);
        int card_index = intent.getIntExtra(DeleteCardActivity.CARD_INDEX, 0);

        //initialise the lists
        cardList = new ArrayList<>();
        card_id_list = new ArrayList<>();

        //initialise the databases
        databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deck_id).child(card_id); // used to delete
        databaseDecks = FirebaseDatabase.getInstance().getReference("deck").child(deck_id); //used to update
        databaseCardlist = FirebaseDatabase.getInstance().getReference("cards").child(deck_id);
        databaseTest = FirebaseDatabase.getInstance().getReference("cards");

        //find widgets
        textView_ConfirmDeleteCardQTAG = (TextView) findViewById(R.id.textView_ConfirmDeleteCardQTAG);
        textView_ConfirmDeleteCardQfill = (TextView) findViewById(R.id.textView_ConfirmDeleteCardQfill);
        textView_ConfirmDeleteCardATAG = (TextView) findViewById(R.id.textView_ConfirmDeleteCardATAG);
        textView_ConfirmDeleteCardAfill = (TextView) findViewById(R.id.textView_ConfirmDeleteCardAfill);
        textView_ConfirmDeleteCardWarning = (TextView) findViewById(R.id.textView_ConfirmDeleteCardWarning);
        button_ConfirmDeleteCardConfirm = (Button) findViewById(R.id.button_ConfirmDeleteCardConfirm);
        button_ConfirmDeleteCardCancel = (Button) findViewById(R.id.button_ConfirmDeleteCardCancel);

        //set the question and answer to the textview
        textView_ConfirmDeleteCardAfill.setText(card_answer);
        textView_ConfirmDeleteCardQfill.setText(card_question);

        //set the value of the dynamic size
        dynamic_size = deck_size;

        //set a listener to button confirm
        button_ConfirmDeleteCardConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create an empty card instance
                Card card = new Card();

                //delete the card from the database
                card.deleteCard(card_id, deck_id);

                //identify the removed card ID
                removed_id = card_id;

                //decrement the decksize
                dynamic_size = dynamic_size - 1;

                //notify the user
                Toast.makeText(ConfirmDeleteCardActivity.this, "Card is deleted", Toast.LENGTH_SHORT).show();

                //create an empty deck instance
                Deck deck = new Deck();

                //update the deck size after card deletion
                deck.updateAfterDelete(deck_id, deck_name, dynamic_size);

                //notify user
                Toast.makeText(ConfirmDeleteCardActivity.this, "Deck updated", Toast.LENGTH_SHORT).show();

                //update the indexes of the cards after card deletion
                card.updateAfterDelete(cardList, removed_id, deck_id);

                //move to home activity
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                Log.d(TAG, "ConfirmClick - out");
                startActivity(intent);
            }
        });

        //set a listener to button cancel
        button_ConfirmDeleteCardCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to home activity
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
            }
        });
        Log.d(TAG, "onCreate: out");
    }


    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: in");
        super.onStart();

        //set an event listener for real time database
        databaseCardlist.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange: in");

                //reset the list to avoid redundant information
                cardList.clear();
                card_id_list.clear();

                for (DataSnapshot cardsSnapshot : dataSnapshot.getChildren()) {
                    //record the card from data snapshot
                    Card card = cardsSnapshot.getValue(Card.class);

                    //create a situation where the card with the removed card id will not be added to the list
                    if (removed_id != card.getCardID()) {
                        Log.d(TAG, "onDataChange if in");
                        card_id_list.add(card.getCardID());
                        cardList.add(card);
                    } else {
                        Log.d(TAG, "onDataChange: else in");
                        continue;
                    }
                    Log.d(TAG, "onDataChange: for_out");
                }
                Log.d(TAG, "onDataChange: out");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ConfirmDeleteCardActivity.this, "Database Error", Toast.LENGTH_SHORT).show();
            }
        });
        Log.d(TAG, "onStart: out");
    }
}

//update the value of the index after deletion
