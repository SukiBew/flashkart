package academy.learnprogramming.flashkart;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class Cardlist extends ArrayAdapter<Card> {

    public static final String TAG = "Flashkart";
    private Activity context;
    private List<Card> cardList;

    public Cardlist(Activity context, List<Card> cardList) {
        super(context, R.layout.cardlist_layout, cardList);
        this.context = context;
        this.cardList = cardList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Log.d(TAG, "getView: in");
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.cardlist_layout, null, true);
        TextView textViewQuestion = (TextView) listViewItem.findViewById(R.id.textView_questionlist);

        Card card = cardList.get(position);

        textViewQuestion.setText(card.getQuestion());
        Log.d(TAG, "getView: out");
        return listViewItem;
    }
}
