package academy.learnprogramming.flashkart;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DefineDeckDialog extends AppCompatActivity {

    public static final String TAG = "Flashkart";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";
    public static final String CARD_ID = "cardID";
    public static final String CARD_INDEX = "cardIndex";
    public static final String CARD_QUESTION = "question";
    public static final String CARD_ANSWER = "answer";


    EditText editText_Deckname;
    Button button_confirm;
    String temp_Deckname;

    DatabaseReference mDeckRef;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: in");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_define_deck_dialog);

        //find widget
        mDeckRef = FirebaseDatabase.getInstance().getReference("deck");
        button_confirm = (Button) findViewById(R.id.button_confirm);
        editText_Deckname = (EditText) findViewById(R.id.editText_Deckname);

        //set button click listener
        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: confirm-in");

                //retrieve information from edit text widget
                temp_Deckname = editText_Deckname.getText().toString();

                //create a condition where the edit text widget cannot be empty
                if (!TextUtils.isEmpty(temp_Deckname)) {
                    //create an empty deck instance
                    Deck deck = new Deck();

                    //create a deck and save it to the database
                    deck.createDeck(temp_Deckname);

                    //move to home activity
                    Intent i = new Intent(view.getContext(), HomeActivity.class);
                    startActivity(i);

                    //notify user
                    Toast.makeText(view.getContext(), "Deck recorded", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onClick: confirm-out");
                } else {
                    //notify user
                    Toast.makeText(view.getContext(), "Please enter the deck name", Toast.LENGTH_SHORT).show();
                }
            }
        });
        Log.d(TAG, "onCreate: out");
    }
}
