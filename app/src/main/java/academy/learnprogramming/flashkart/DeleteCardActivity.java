package academy.learnprogramming.flashkart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DeleteCardActivity extends AppCompatActivity {

    public static final String TAG = "Flashkart";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";
    public static final String CARD_ID = "cardID";
    public static final String CARD_INDEX = "cardIndex";
    public static final String CARD_QUESTION = "question";
    public static final String CARD_ANSWER = "answer";

    DatabaseReference databaseCards;
    DatabaseReference databaseDecks;

    TextView textView_DeleteCardDeckname;
    TextView textView_DeleteCardTAG;
    ListView listView_DeleteCardCardlist;
    List<Card> cardList;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: in");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_card);

        //retrieve information from previous activity
        Intent intent = getIntent();
        final String deckid = intent.getStringExtra(OptionActivity.DECK_ID);
        final String deckname = intent.getStringExtra(OptionActivity.DECK_NAME);
        final int decksize = intent.getIntExtra(OptionActivity.DECK_SIZE, 0);

        //initialise the database
        databaseDecks = FirebaseDatabase.getInstance().getReference("deck");
        databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deckid);

        //find widgets
        textView_DeleteCardDeckname = (TextView) findViewById(R.id.textView_DeleteCardDeckname);
        textView_DeleteCardTAG = (TextView) findViewById(R.id.textView_DeleteCardTAG);
        listView_DeleteCardCardlist = (ListView) findViewById(R.id.listView_DeleteCardCardlist);

        //set the deckname to the textview
        textView_DeleteCardDeckname.setText(deckname);

        //initialise the list
        cardList = new ArrayList<>();

        //set a listener to for the items inside the list
        listView_DeleteCardCardlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //identify the clicked items
                Card card = cardList.get(position);

                //move to confirm delete card activity along with relevant information
                Intent intent = new Intent(getApplicationContext(), ConfirmDeleteCardActivity.class);
                intent.putExtra(CARD_ID, card.getCardID());
                intent.putExtra(CARD_ANSWER, card.getAnswer());
                intent.putExtra(CARD_QUESTION, card.getQuestion());
                intent.putExtra(CARD_INDEX, card.getCardIndex());
                intent.putExtra(DECK_NAME, deckname);
                intent.putExtra(DECK_ID, deckid);
                intent.putExtra(DECK_SIZE, decksize);
                Log.d(TAG, "onItemClick: ds: " + decksize);
                startActivity(intent);
            }
        });
        Log.d(TAG, "onCreate: out");
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: in");
        super.onStart();

        //set a event listener for real time database
        databaseCards.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange: in");

                //clear the list to avoid redundant information
                cardList.clear();

                for (DataSnapshot cardsSnapshot : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: for_in");
                    //retrieve the stored value from data snap shot
                    Card card = cardsSnapshot.getValue(Card.class);

                    //add the card to the card list
                    cardList.add(card);
                    Log.d(TAG, "onDataChange: for_out");
                }
                //set an adapter for the list view
                Cardlist adapter = new Cardlist(DeleteCardActivity.this, cardList);
                listView_DeleteCardCardlist.setAdapter(adapter);
                Log.d(TAG, "onDataChange: out");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(DeleteCardActivity.this, "Database Error", Toast.LENGTH_SHORT).show();
            }
        });
        Log.d(TAG, "onStart: out");
    }
}
