package academy.learnprogramming.flashkart;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayActivity extends AppCompatActivity {

    //initialise external & global variable
    public static final String TAG = "Flashkart";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";
    public static final String CARD_ID = "cardID";
    public static final String CARD_INDEX = "cardIndex";
    public static final String CARD_QUESTION = "question";
    public static final String CARD_ANSWER = "answer";
    public static final int STARTING_INDEX = 1;


    //initialise the widget
    private TextSwitcher textSwitcher_PlayQAFill;
    private TextView textView_PlayDeckname;
    private TextView textView_PlayCurrentIndex;
    private TextView textView_PlayDecksize;
    private TextView textView_PlayQATag;
    private TextView testTextView;
    private TextView textView_PlayDivider;
    private Button button_PlayNext;
    private Button button_PlayShowAnswer;

    DatabaseReference databaseDeck;
    private DatabaseReference databaseCards;
    private List<Card> cardList;
    private List<String> card_id_list = new ArrayList();


    private int current_index = 1;
    private String string_currentIndex = Integer.toString(current_index);
    //indicator 0 = Question / 1 = Answer;
    private String question;
    private String answer;
    private String string_deck_size;
    private String[] QA = new String[2];
    private int index = 0;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: in");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        //find all of the widget

        textView_PlayDeckname = (TextView) findViewById(R.id.textView_PlayDeckname);
        textView_PlayCurrentIndex = (TextView) findViewById(R.id.textView_PlayCurrentIndex);
        textView_PlayDecksize = (TextView) findViewById(R.id.textView_PlayDecksize);
        textView_PlayQATag = (TextView) findViewById(R.id.textView_PlayQATag);
        textView_PlayDivider = (TextView) findViewById(R.id.textView_PlayDivider);
        textSwitcher_PlayQAFill = (TextSwitcher) findViewById(R.id.textSwitcher_PlayQAFill);
        button_PlayNext = (Button) findViewById(R.id.button_PlayNext);
        button_PlayShowAnswer = (Button) findViewById(R.id.button_PlayShowAnswer);
        //setActivityBackgroundColor(Color.parseColor("#ffa500"));
        cardList = new ArrayList<>();

        //retrieve the information passed from the previous activity
        Intent intent = getIntent();
        final String deck_id = intent.getStringExtra(OptionActivity.DECK_ID);
        final String deck_name = intent.getStringExtra(OptionActivity.DECK_NAME);
        final int deck_size = intent.getIntExtra(OptionActivity.DECK_SIZE, 0);
        string_deck_size = Integer.toString(deck_size);
        databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deck_id);

        textView_PlayDeckname.setText(deck_name);
        textView_PlayDecksize.setText(string_deck_size);
        textView_PlayCurrentIndex.setText(string_currentIndex);
        textSwitcher_PlayQAFill.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                testTextView = new TextView(PlayActivity.this);
                testTextView.setTextColor(Color.BLACK);
                testTextView.setTextSize(60);
                testTextView.setGravity(Gravity.CENTER_HORIZONTAL);
                return testTextView;
            }
        });

        button_PlayShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: in");
                //read the indicator
                if(index == 0){
                    //setActivityBackgroundColor(16753920);
                }
                else{
                    //setActivityBackgroundColor(16766720);
                }
                index = display(index);
                Log.d(TAG, "onClick: out");

            }
        });

        button_PlayNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = 0;
                getQA(card_id_list, cardList);
                index = display(index);
                current_index = current_index + 1;
                if (current_index <= deck_size) {
                    string_currentIndex = Integer.toString(current_index);
                    textView_PlayCurrentIndex.setText(string_currentIndex);
                } else {
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    Toast.makeText(PlayActivity.this, "Cram Complete", Toast.LENGTH_LONG).show();
                }
            }
        });
        Log.d(TAG, "onCreate: out");
    }


    @Override
    protected void onStart() {
        super.onStart();

        databaseCards.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) { // will launch everything we change anything inside the database
                Log.d(TAG, "onDataChange: in");
                card_id_list.clear(); //reset the list

                for (DataSnapshot cardsSnapshot : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: for_in");
                    //populate the list to generate a list of card ID to be randomise

                    Card card = cardsSnapshot.getValue(Card.class);
                    String cardid = card.getCardID();

                    Log.d(TAG, "onDataChange: card-id: " + cardid);
                    card_id_list.add(cardid);
                    cardList.add(card);
                    for (String id : card_id_list) {
                        Log.d(TAG, "cardIdListVer: " + id);
                    }

                    Log.d(TAG, "onDataChange: for_out");
                }
                //randomise the order of the deck
                Collections.shuffle(card_id_list);
                getQA(card_id_list, cardList);
                index = display(index);
                //play(card_id_list,cardList);
                // display();
                Log.d(TAG, "onDataChange: out");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { //will launch whenever there is an error occurs

            }
        });
    }

    private int display(int index) {
        Log.d(TAG, "display: in");
        Log.d(TAG, "indicator: " + index);
        if (index == 0) {
            String showa = "Show Answer";
            setActivityBackgroundColor(Color.parseColor("#ffa500"));
            Log.d(TAG, "question: " + question);
            textSwitcher_PlayQAFill.setText(question);
            button_PlayShowAnswer.setText(showa);
            index = index + 1;
        } else if (index == 1) {
            String showq = "Show Question";
            setActivityBackgroundColor(Color.parseColor("#ffd700"));
            Log.d(TAG, "answer: " + answer);
            textSwitcher_PlayQAFill.setText(answer);
            button_PlayShowAnswer.setText(showq);
            index = index - 1;
        } else {
            Toast.makeText(PlayActivity.this, "index: " + index, Toast.LENGTH_SHORT);
            index = 0;
        }
        Log.d(TAG, "display: out");
        return index;
    }

    private String[] getQA(List<String> card_id_list, List<Card> cardList) {
        Log.d(TAG, "getQA: in");
        int initialsize = card_id_list.size();
        for (int i = initialsize - 1; i >= 0; i--) {
            question = null;
            Log.d(TAG, "index i: " + i);
            Log.d(TAG, "getQA: for1");
            String id = card_id_list.get(i);
            for (Card card : cardList) {
                Log.d(TAG, "getQA: for2");
                if (card.getCardID() == id) {
                    question = card.getQuestion();
                    answer = card.getAnswer();
                    Log.d(TAG, "answer: " + answer);
                    Log.d(TAG, "question: " + question);
                    QA[0] = question;
                    QA[1] = answer;
                    card_id_list.remove(id);
                    break;
                }
            }
            if (question != null) {
                break;
            }
        }
        Log.d(TAG, "getQA: out");
        return QA;
    }

    public void setActivityBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(color);
    }
}

//create a list of card ID
//randomise the list
//function to get the question and answer from a specific ID
//remove ID from the list
//show the list

