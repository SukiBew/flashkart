package academy.learnprogramming.flashkart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class OptionActivity extends AppCompatActivity {

    public static final String TAG = "Flashkart";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";
    public static final String CARD_ID = "cardID";
    public static final String CARD_INDEX = "cardIndex";
    public static final String CARD_QUESTION = "question";
    public static final String CARD_ANSWER = "answer";

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);

        //find widgets
        Button buttonAdd = (Button) findViewById(R.id.buttonAdd);
        Button buttonPlay = (Button) findViewById(R.id.buttonPlay);
        Button buttonEdit = (Button) findViewById(R.id.buttonEdit);
        Button buttonDeleteCards = (Button) findViewById(R.id.buttonDeleteCards);
        Button buttonDeleteDeck = (Button) findViewById(R.id.buttonDeleteDeck);

        //receive the information passed from the previous activity
        Intent intent = getIntent();
        final String id = intent.getStringExtra(HomeActivity.DECK_ID);
        Log.d(TAG, "deckid : " + id);
        final String deckname = intent.getStringExtra(HomeActivity.DECK_NAME);
        Log.d(TAG, "deckname : " + deckname);
        final int decksize = intent.getIntExtra(HomeActivity.DECK_SIZE, 0);
        Log.d(TAG, "decksize : " + decksize);



        //set a listener to the button delete
        buttonDeleteDeck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to delete deck activity along with relevant information
                Intent intent = new Intent(getApplicationContext(), DeleteDeckActivity.class);
                intent.putExtra(DECK_ID, id);
                intent.putExtra(DECK_NAME, deckname);
                intent.putExtra(DECK_SIZE, decksize);
                startActivity(intent);
            }
        });

        //set a listener to delete cards button
        buttonDeleteCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to delete card activity along with relevant information
                Intent intent = new Intent(getApplicationContext(), DeleteCardActivity.class);
                intent.putExtra(DECK_ID, id);
                intent.putExtra(DECK_NAME, deckname);
                intent.putExtra(DECK_SIZE, decksize);
                startActivity(intent);
            }
        });

        //set a listener to play button
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to play activity along with relevant information
                Intent intent = new Intent(getApplicationContext(), PlayActivity.class);
                intent.putExtra(DECK_ID, id);
                intent.putExtra(DECK_NAME, deckname);
                intent.putExtra(DECK_SIZE, decksize);
                startActivity(intent);
            }
        });

        //set a listener to button edit
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to edit menu activity along with relevant information
                Intent intent = new Intent(getApplicationContext(), EditMenuActivity.class);
                intent.putExtra(DECK_ID, id);
                intent.putExtra(DECK_NAME, deckname);
                intent.putExtra(DECK_SIZE, decksize);
                startActivity(intent);
            }
        });

        //set a listener to button add
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to create deck activity along with relevant information
                Intent intent = new Intent(getApplicationContext(), CreateDeckActivity.class);
                intent.putExtra(DECK_ID, id);
                intent.putExtra(DECK_NAME, deckname);
                intent.putExtra(DECK_SIZE, decksize);
                startActivity(intent);
            }
        });
    }
}
