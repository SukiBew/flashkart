package academy.learnprogramming.flashkart;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.content.Intent;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends AppCompatActivity {

    public static final String TAG = "Flashkart";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";

    DatabaseReference mDeckRef;
    private ListView listView;

    List<Deck> deckList;
    ListView listViewDecks;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: onCreate In");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //find widgets
        mDeckRef = FirebaseDatabase.getInstance().getReference("deck");
        listView = (ListView) findViewById(R.id.listViewSimple);
        Button button_add = (Button) findViewById(R.id.button_add);
        listViewDecks = (ListView) findViewById(R.id.listViewSimple);
        deckList = new ArrayList<>();

        //set a listener to the button add
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), DefineDeckDialog.class);
                startActivity(intent);
            }
        });

        //set a listener to the item in the list
        listViewDecks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //identify the selected deck
                Deck deck = deckList.get(position);

                //move to option activity along with few relevant information
                Intent intent = new Intent(getApplicationContext(), OptionActivity.class);
                intent.putExtra(DECK_ID, deck.getId());
                intent.putExtra(DECK_NAME, deck.getDeckName());
                intent.putExtra(DECK_SIZE, deck.getDeckSize());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        //add an event listener to the activity for real time database
        mDeckRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) { // will launch everything we change anything inside the database
                //reset the list to avoid redundant information
                deckList.clear();

                //loop the snapshot to record all of the decks available in the database
                for (DataSnapshot decksSnapshot : dataSnapshot.getChildren()) {
                    Deck deck = decksSnapshot.getValue(Deck.class);
                    deckList.add(deck);
                }

                //set the adapter for the list view
                Decklist adapter = new Decklist(HomeActivity.this, deckList);
                listViewDecks.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { //will launch whenever there is an error occurs
                //notify user
                Toast.makeText(HomeActivity.this, "Database Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
