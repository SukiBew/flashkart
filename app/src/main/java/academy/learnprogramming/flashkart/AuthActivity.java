package academy.learnprogramming.flashkart;

import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class AuthActivity extends AppCompatActivity {
    private static final String TAG = "SignInActivity";
    public static final String USER_NAME = "userName";
    public static final String USER_ID = "userID";

    private static final int RC_SIGN_IN = 9001;
    SignInButton button_SignIn;
    Button button_SignOut;
    TextView textView_appTitle;
    GoogleApiClient mGoogleApiClient;
    TextView statusTextView;
    ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        //initialise google sign in option
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, null /* FragmentActivity */ /*OnConnectionFailedListener*/)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //find widgets
        icon = (ImageView) findViewById(R.id.imageView);
        statusTextView = (TextView) findViewById(R.id.textView_StatusTextView);
        button_SignIn = (SignInButton) findViewById(R.id.button_SignIn);
        textView_appTitle = (TextView) findViewById(R.id.textView_appTitle);
        button_SignOut = (Button) findViewById(R.id.button_SignOut);

        //set a listener to button sign in
        button_SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button_SignIn:
                        signIn();
                        break;
                    case R.id.button_SignOut:
                        signOut();
                        break;
                }
            }
        });

        //set a listener to button sign out
        button_SignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button_SignIn:
                        signIn();
                        break;
                    case R.id.button_SignOut:
                        signOut();
                        break;
                }
            }
        });
    }

    //create a sign in function
    private void signIn() {
        //move to home activity if sign in successful
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Result returned from launching the intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult: " + result.isSuccess());
        //signed in successfully, show authenticated UI.
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            statusTextView.setText("Hello, " + acct.getDisplayName());

            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        } else {
        }
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: " + connectionResult);
    }

    //create a sign out function
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                statusTextView.setText("Signed out");
            }
        });
    }
}
