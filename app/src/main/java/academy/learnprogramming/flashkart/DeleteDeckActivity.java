package academy.learnprogramming.flashkart;

import android.content.Intent;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DeleteDeckActivity extends AppCompatActivity {

    public static final String TAG = "Flashkart";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";
    public static final String CARD_ID = "cardID";
    public static final String CARD_INDEX = "cardIndex";
    public static final String CARD_QUESTION = "question";
    public static final String CARD_ANSWER = "answer";

    DatabaseReference databaseDeck;
    DatabaseReference databaseCard;
    TextView textView_DeleteDeckWarning;
    TextView textView_DeleteDeckDeckname;
    Button button_DeleteDeckConfirm;
    Button button_DeleteDeckCancel;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_deck);

        //initialise database
        databaseDeck = FirebaseDatabase.getInstance().getReference("deck");
        databaseCard = FirebaseDatabase.getInstance().getReference("cards");

        //find widgets
        textView_DeleteDeckDeckname = (TextView) findViewById(R.id.textView_DeleteDeckDeckname);
        textView_DeleteDeckWarning = (TextView) findViewById(R.id.textView_DeleteDeckWarning);
        button_DeleteDeckCancel = (Button) findViewById(R.id.button_DeleteDeckCancel);
        button_DeleteDeckConfirm = (Button) findViewById(R.id.button_DeleteDeckConfirm);

        //retrieve information from previous activity
        Intent intent = getIntent();
        final String deck_id = intent.getStringExtra(OptionActivity.DECK_ID);
        final String deckname = intent.getStringExtra(OptionActivity.DECK_NAME);
        final int decksize = intent.getIntExtra(OptionActivity.DECK_SIZE, 0);

        //set deckname to the textview
        textView_DeleteDeckDeckname.setText(deckname);

        //set a click listener to confirm button
        button_DeleteDeckConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create empty instances of card and deck
                Deck deck = new Deck();
                Card card = new Card();

                //delete the selected deck
                card.deleteAllCards(deck_id);

                //delete all of the cards with the selected deck id
                deck.deleteDeck(deck_id);

                //notify user
                Toast.makeText(DeleteDeckActivity.this, "Deck is deleted", Toast.LENGTH_SHORT).show();

                //move the home activity
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
            }
        });

        //set a listener to the cancel button
        button_DeleteDeckCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to home activity
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
