package academy.learnprogramming.flashkart;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class CreateDeckActivity extends AppCompatActivity {

    public static final String TAG = "Flashkart-CreateDeck";
    public static final String DECK_SIZE = "decksize";
    public static final String STARTING_INDEX = "1";

    TextView deckName_TextView;
    TextView question_TextView;
    TextView answer_TextView;
    TextView index_TextView;
    TextView indexNum_TextView;
    EditText question_EditText;
    EditText answer_EditText;
    Button button_Next;
    Button button_Complete;

    DatabaseReference databaseCards;
    List<Card> cardList;


    private String question;
    private String answer;
    private int cardIndex; //to be counted
    private String string_cardIndex;  //to be displayed
    private int decksize;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: in");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_deck);

        //find widgets
        deckName_TextView = (TextView) findViewById(R.id.textView_CreateDeckDeckname);
        question_TextView = (TextView) findViewById(R.id.textView_CreateDeckQuestion);
        answer_TextView = (TextView) findViewById(R.id.textView_CreateDeckDeckname);
        index_TextView = (TextView) findViewById(R.id.textView_CreateDeckIndex);
        indexNum_TextView = (TextView) findViewById(R.id.textView_CreateDeckIndexNum);
        question_EditText = (EditText) findViewById(R.id.editText_CreateDeckQuestion);
        answer_EditText = (EditText) findViewById(R.id.editText_CreateDeckAnswer);
        button_Next = (Button) findViewById(R.id.button_CreateDeckNext);
        button_Complete = (Button) findViewById(R.id.button_CreateDeckComplete);

        //initialise list
        cardList = new ArrayList<>();
        //create an empty card instance
        final Card card = new Card();

        //receive information from previous activity
        Intent intent = getIntent();
        final String deck_id = intent.getStringExtra(HomeActivity.DECK_ID);
        final String deckname = intent.getStringExtra(HomeActivity.DECK_NAME);

        //set the deckname to the deckname textview
        deckName_TextView.setText(deckname);

        databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deck_id);

        //setting up a listener for button next
        button_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ensure that both of the edit text widget is not empty
                question = question_EditText.getText().toString();
                answer = answer_EditText.getText().toString();

                if (!TextUtils.isEmpty(question) && !TextUtils.isEmpty(answer)) {
                    //retrieve the information from the widget
                    String cardID = databaseCards.push().getKey();
                    string_cardIndex = indexNum_TextView.getText().toString();

                    //increment the index
                    cardIndex = (Integer.parseInt(string_cardIndex)) + 1;

                    card.saveCard(question, answer, deck_id, cardIndex);

                    //notify the user
                    Toast.makeText(view.getContext(), "Card recorded", Toast.LENGTH_SHORT).show();

                    //update the index
                    string_cardIndex = Integer.toString(cardIndex);
                    indexNum_TextView.setText(string_cardIndex);

                    //Clear the text view for next input
                    answer_EditText.setText("");
                    question_EditText.setText("");
                }
                else{
                    //notify the user
                    Toast.makeText(view.getContext(), "Question or Answer should not be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //set a listener for button complete
        button_Complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //retrieve the current index as decksize
                decksize = Integer.parseInt(indexNum_TextView.getText().toString());

                //create an empty deck instance
                Deck deck = new Deck();

                //update the deck database
                deck.updateDeck(deck_id, deckname, decksize);

                //notify the user
                Toast.makeText(view.getContext(), "Deck updated", Toast.LENGTH_SHORT).show();

                //move to home activity
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
            }
        });
        Log.d(TAG, "onCreate: out");
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: in");
        super.onStart();

        //set index number
        indexNum_TextView.setText(STARTING_INDEX);

        //set an event listener for real time database
        databaseCards.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) { // will launch everything we change anything inside the database
                Log.d(TAG, "onDataChange: in");
                //reset card list to avoid redundant information
                cardList.clear(); //reset the list

                for (DataSnapshot decksSnapshot : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: for-in");

                    //retrieve the information from the data snapshot
                    Card card = decksSnapshot.getValue(Card.class);

                    //insert the recorded card to the list
                    cardList.add(card);

                    //decrement the index
                    int live_cardIndex = 1;

                    //determine the decksize
                    if (live_cardIndex <= card.getCardIndex()) {
                        cardIndex = card.getCardIndex();
                        string_cardIndex = Integer.toString(cardIndex);
                        indexNum_TextView.setText(string_cardIndex);
                        decksize = live_cardIndex;
                    } else {
                        string_cardIndex = Integer.toString(live_cardIndex);
                        indexNum_TextView.setText(string_cardIndex);
                        decksize = live_cardIndex;
                    }
                    Log.d(TAG, "onDataChange: for-out");
                }
                //parse the index to string
                string_cardIndex = Integer.toString(cardIndex);
                //set the string index to the textview
                indexNum_TextView.setText(string_cardIndex); //setText only accept string as parimeter
                Log.d(TAG, "onDataChange: out");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { //will launch whenever there is an error occurs
                //notify user
                Toast.makeText(CreateDeckActivity.this, "Please enter the deck name", Toast.LENGTH_SHORT).show();
            }
        });
        Log.d(TAG, "onStart: out");
    }
}
