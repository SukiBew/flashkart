package academy.learnprogramming.flashkart;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class Deck {
    int deckSize;
    String id;
    String deckName;

    DatabaseReference databaseDeck;

    public void createDeck(String deckName){
        databaseDeck = FirebaseDatabase.getInstance().getReference("deck");
            String id = databaseDeck.push().getKey();
            Deck deck = new Deck(id, deckName, 0);
            databaseDeck.child(id).setValue(deck);
    }

    public void updateDeck(String id, String deckname, int decksize) {
        DatabaseReference databaseDeck = FirebaseDatabase.getInstance().getReference("deck").child(id);
        Deck deck = new Deck(id, deckname, decksize);
        databaseDeck.setValue(deck);
    }

    public void updateAfterDelete(String deck_id, String deck_name, int dynamic_size) {
        databaseDeck = FirebaseDatabase.getInstance().getReference("deck").child(deck_id);
        Deck deck = new Deck(deck_id, deck_name, dynamic_size);
        databaseDeck.setValue(deck);
    }

    public void deleteDeck(String deck_id){
        databaseDeck = FirebaseDatabase.getInstance().getReference("deck");
        databaseDeck.child(deck_id).removeValue();
    }

    public Deck() {

    }

    public Deck(String id, String deckName, int deckSize) {
        this.id = id;
        this.deckName = deckName;
        this.deckSize = deckSize;
    }


    public Deck(int deckSize, String id, String deckName, ArrayList<Card> cards) {
        this.id = id;
        this.deckSize = deckSize;
        this.deckName = deckName;
    }

    public int getDeckSize() {
        return deckSize;
    }

    public void setDeckSize(int deckSize) {
        this.deckSize = deckSize;
    }

    public String getDeckName() {
        return deckName;
    }

    public void setDeckName(String deckName) {
        this.deckName = deckName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
