package academy.learnprogramming.flashkart;

import android.nfc.Tag;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class Card {

    String cardID;
    int cardIndex;
    String question;
    String answer;

    DatabaseReference databaseCards;

    public void saveCard(String question, String answer, String deckID, int cardIndex){
           databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deckID);
           String cardID = databaseCards.push().getKey();
           Card card = new Card(cardID, cardIndex, question, answer);
           databaseCards.child(cardID).setValue(card);
    }

    public void updateCard(String card_id, int card_index, String card_question, String card_answer, String deckid) {
        databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deckid).child(card_id);
        Card card = new Card(card_id, card_index, card_question, card_answer);
        databaseCards.setValue(card);
    }

    public void deleteCard(String card_id, String deck_id) {
        databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deck_id);
        databaseCards.child(card_id).removeValue();
    }

    public void deleteAllCards(String deck_id){
        databaseCards = FirebaseDatabase.getInstance().getReference("cards");
        databaseCards.child(deck_id).removeValue();
    }

    public void updateAfterDelete(List<Card> cardList, String removed_id, String deck_id) {
        //empty the list of cards
        databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deck_id);
        int counter = 1;
        for (Card card : cardList) {
            String container = card.getCardID();
            if (!container.equals(removed_id)) {
                String cardid = card.getCardID();
                databaseCards.child(cardid).child("cardIndex").setValue(counter);
                counter = counter + 1;
            } else {
                continue;
            }
        }
    }

    public Card(){

    }

    public Card(String cardID, int cardIndex, String question, String answer) {
        this.cardID = cardID;
        this.cardIndex = cardIndex;
        this.question = question;
        this.answer = answer;
    }

    public int getCardIndex() {
        return cardIndex;
    }

    public void setCardIndex(int cardIndex) {
        this.cardIndex = cardIndex;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }


}

