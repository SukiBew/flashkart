package academy.learnprogramming.flashkart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class EditMenuActivity extends AppCompatActivity {

    public static final String TAG = "Flashkart";
    public static final String DECK_NAME = "deckname";
    public static final String DECK_ID = "deckid";
    public static final String DECK_SIZE = "decksize";
    public static final String CARD_ID = "cardID";
    public static final String CARD_INDEX = "cardIndex";
    public static final String CARD_QUESTION = "question";
    public static final String CARD_ANSWER = "answer";

    DatabaseReference databaseCards;
    TextView textView_questionMenu;
    TextView textView_deckName;
    ListView listViewQuestions;
    List<Card> cardList;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editmenu);

        //retrieve information from previous activity
        Intent intent = getIntent();
        final String deckid = intent.getStringExtra(OptionActivity.DECK_ID);
        final String deckname = intent.getStringExtra(OptionActivity.DECK_NAME);

        //initialise database
        databaseCards = FirebaseDatabase.getInstance().getReference("cards").child(deckid);

        //find widgets
        textView_questionMenu = (TextView) findViewById(R.id.textView_QuestionMenu);
        textView_deckName = (TextView) findViewById(R.id.textView_EditMenudeckname);
        listViewQuestions = (ListView) findViewById(R.id.listview_EditMenuQuestionList);

        //initialise list
        cardList = new ArrayList<>();

        //set a item click listener
        listViewQuestions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //identify the position of the clicked item
                Card card = cardList.get(position);

                //Intent intent = new Intent(getApplicationContext(), CreateDeckActivity.class);
                Intent intent = new Intent(getApplicationContext(), EditCardActivity.class);
                intent.putExtra(CARD_ID, card.getCardID());
                intent.putExtra(CARD_ANSWER, card.getAnswer());
                intent.putExtra(CARD_QUESTION, card.getQuestion());
                intent.putExtra(CARD_INDEX, card.getCardIndex());
                intent.putExtra(DECK_NAME, deckname);
                intent.putExtra(DECK_ID, deckid);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart: in");
        super.onStart();

        //set up an event listener for the database for real time database
        databaseCards.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) { // will launch everything we change anything inside the database
                Log.d(TAG, "onDataChange: in");
                cardList.clear(); //reset the list

                for (DataSnapshot cardsSnapshot : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: for_in");
                    Card card = cardsSnapshot.getValue(Card.class);
                    cardList.add(card);
                    //Log.d(TAG, "cardQuestion:" + card.getQuestion());
                    Log.d(TAG, "onDataChange: for_out");
                }

                Cardlist adapter = new Cardlist(EditMenuActivity.this, cardList);
                listViewQuestions.setAdapter(adapter);
                Log.d(TAG, "onDataChange: out");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { //will launch whenever there is an error occurs
                //notify user
                Toast.makeText(EditMenuActivity.this, "Please enter the deck name", Toast.LENGTH_SHORT).show();
            }
        });
        Log.d(TAG, "onStart: out");
    }
}
