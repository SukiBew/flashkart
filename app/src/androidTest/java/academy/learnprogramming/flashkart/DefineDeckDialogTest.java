package academy.learnprogramming.flashkart;


import android.support.test.rule.ActivityTestRule;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


public class DefineDeckDialogTest {

    DatabaseReference databaseDeck;
    String test = "Test";

    @Rule
    public ActivityTestRule<DefineDeckDialog> activityTestRule =
            new ActivityTestRule<>(DefineDeckDialog.class);

    @Test
    public void createDeckTest(){
        databaseDeck = FirebaseDatabase.getInstance().getReference("deck").child(test);

        onView(withId(R.id.editText_Deckname))
                .perform(typeText(test), closeSoftKeyboard());
        onView(withId(R.id.button_confirm))
                .perform(click());
        assertNotNull(databaseDeck);
    }

    @After
    public void tearDown() throws Exception{
        databaseDeck.removeValue();
    }
}